TFNG Get Started Session 1
==========================

*Get started with Eclipse, Maven and REST*

Setup and Configure Eclipse IDE
-------------------------------

### Download

1.  [JDK 1.8](a.%09http:/www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (Java SE Development Kit 8u181, jdk-8u181-windows-x64.exe)

2.  [Eclipse IDE](http://www.eclipse.org/downloads/packages/) (Eclipse IDE for Java EE Developers, Windows 64-bit)

3.  [Tomcat 9.0](https://tomcat.apache.org/download-90.cgi) (Core: 64-bit Windows zip)

### Setup JDK 1.8

1.  Install JDK 1.8 with the location of

    1.  JDK = C:\\Program Files\\Java\\jdk1.8

    2.  JRE = C:\\Program Files\\Java\\jre1.8

### Setup Eclipse IDE

1.  Download and extract Eclipse IDE to C:\\Programs\\Eclipse

2.  Run Eclipse IDE

3.  In Eclipse IDE menu, select Windows \> Preferences \> Java \> Installed JREs

4.  Select jre-1.8 (default), then click on Edit

5.  Enter JRE home = C:\\Program Files\\Java\\jdk1.8 and JRE name = jdk1.8, then
    Finish

![](get-started-1/de2648143fbc2ba9a1320145c00772c3.png)

### Configure Tomcat 9.0 in Eclipse IDE

1.  Download and extract Tomcat 9 to C:\\Programs\\Tomcat-9.0

2.  In Eclipse IDE menu, select Windows \> Preferences \> Server \> Runtime
    Environments

3.  Click Add, select Apache Tomcat v9.0 and check Create a new local server,
    then Next

4.  Enter the Tomcat Installation Directory = C:\\Programs\\Tomcat-9.0, then
    Finish

![](get-started-1/13c715594aa773389ea9763d9c6161b1.png)

### Configure Tomcat Server Timeout in Eclipse IDE

1.  In Eclipse IDE bottom dock, select Servers, then double click Tomcat v9.0
    Server at localhost.

![](get-started-1/83bbec405c55449f59853df49e82d786.png)

2.  Expand Timeouts, change Start (in seconds) = 120 (or more if had timeout
    issues when starting Tomcat)

![](get-started-1/f4f802dcb6e2e783f6b597676c70f926.png)

### Configure Maven in Eclipse IDE

1.  In Eclipse IDE menu, select Windows \> Preferences \> Maven

2.  Tick both Download Artifact Source and Download Artifact Javadoc

![](get-started-1/e197768d84575a32684dfebb6335045a.png)

### Install EditorConfig in Eclipse IDE

1.  In Eclipse IDE menu, Help \> Eclipse Marketplace

2.  Enter Find = editorconfig, then click Go to search

3.  Click install

### Install YEdit in Eclipse IDE

1.  In Eclipse IDE menu, Help \> Eclipse Marketplace

2.  Enter Find = yedit, then click Go to search

3.  Click install

Clone and Import TFNG Get Started REST project in Eclipse IDE
-------------------------------------------------------------

1.  Register an account with <https://gitlab.com/users/sign_in#register-pane>

### Clone TFNG Get Started REST project

1.  In Eclipse IDE menu, select Windows \> Perspective \> Open Perspective \>
    Others

2.  Select Git then OK

![](get-started-1/aa9ddbd2d9ad636eb4b73abe0bdb2c7a.png)

1.  Click Clone a Git Repository

2.  Enter URL = <https://gitlab.com/tfng-get-started/get-started-rest.git>, your
    registered GitLab username and password, then click Next

3.  Select all branches, then click Next

4.  Enter Directory = C:\\Projects\\TFNG\\get-started\\get-started-rest, then
    click Finish

![](get-started-1/fa9494cce6e12bf835b4c0da9cba9517.png)

1.  Right click ‘get-started-rest [master]’ repository, select Switch To \> New
    Branch

2.  Click on Select to choose a branch

3.  Choose Remote Tracking \> origin/1.0 … then click on OK, then click on
    Finish

4.  Your repository is now working under branch 1.0

![](get-started-1/17b2f505202494b1cb1c61cd493730db.png)

### Import Cloned Project into Eclipse IDE

1.  In Eclipse IDE menu, select File \> Import

2.  Select Maven \> Existing Maven Projects

3.  Enter Root Directory = C:\\Projects\\TFNG\\get-started\\get-started-rest

4.  Select all projects, click Finish and wait for Maven to download
    dependencies for the first time

5.  In Eclipse IDE menu, select Windows \> Perspective \> Open Perspective \>
    Others

6.  Select Java EE (default)

7.  You will have 6 projects loaded as below

    1.  TFNG Get Started – Root project

    2.  TFNG Get Started Dependencies - Manages dependency and plugin versions

    3.  TFNG Get Started Projects - Manages child project compile, test, build
        etc. settings

    4.  TFNG Get Started REST Application - Web Application using Banner and
        Greeting library

    5.  TFNG Get Started REST Banner - Library exposing REST GET methods for
        Banner entity

    6.  TFNG Get Started REST Greeting - Library exposing REST GET methods for
        Greeting entity

![](get-started-1/bff2bd4b209b0f341daecc07d000499a.png)

Using Eclipse IDE
-----------------

### Run TFNG Get Started REST Application

1.  In Eclipse IDE menu, select Windows \> Web Browser \> 2. Firefox (or others
    if Firefox not installed)

2.  Right click the project ‘get-started-rest-app’ \> Run As \> Run on Server

3.  Tick the Always use this server when running this project

4.  Click Finish and wait for Tomcat to start

![](get-started-1/e2eb005c633209aa211bf26251a3f0cd.png)

1.  Once Tomcat loaded the project, it will open the browser and redirected to
    URL = <http://localhost:8080/get-started-rest-app/> with a ‘Whitelabel Error
    Page’

2.  Change the URL =
    <http://localhost:8080/get-started-rest-app/api/banner/default> to do a REST
    GET Banner with id=default, then press Enter and you should get response as
    below.

![](get-started-1/536a16af56ecffa66773c676599d6d7b.png)

1.  Change the URL = <http://localhost:8080/get-started-rest-app/api/greetings>
    to do a REST GET all Greeting, then press Enter and you should get response
    as below.

![](get-started-1/efe315133fc1a7fa6e5d94a6e4795183.png)

1.  Change the URL =
    <http://localhost:8080/get-started-rest-app/api/greetings?message=great> to
    do a REST GET all Greeting with message containing ‘great’ ignoring case,
    then press Enter and you should get response as below.

![](get-started-1/00425adcad0ceb4ca97145b30666370c.png)

1.  Change the URL =
    <http://localhost:8080/get-started-rest-app/api/greetings/1> to do a REST
    GET one Greeting with id=1, then press Enter and you should get response as
    below.

![](get-started-1/e3e62098cc57731ab9d3946b24c30497.png)

1.  In Eclipse IDE, click on the Stop button to shut down the server

![](get-started-1/5dfeda48038ddf6eafab36c0826eb813.png)

### Build TFNG Get Started Projects using Maven Build

1.  Right click the project ‘get-started-rest-projects’ \> Run As \> 3. Maven
    Build

2.  Enter Goals = package, click Run and wait for Maven to download dependencies
    for the first time

3.  Wait for build success

![](get-started-1/fd66bc8fdaa8a9890ffd5717668ed898.png)

1.  The build output will be in
    C:\\Projects\\TFNG\\get-started\\get-started-rest\\get-started-rest-projects\\get-started-rest-app\	arget\\get-started-rest-app-1.0.war

### Debug TFNG Get Started REST Banner

1.  Expand project tfng-get-started-rest-banner \> src/main/java \>
    tfng.getstarted.rest.banner

2.  Open source BannerRestController.java

3.  Move your cursor to the statement return bannerService.findById(id);

4.  In Eclipse IDE menu, select Run \> Toggle Breakpoint

![](get-started-1/cdd884f7a21ed3e1cd3354c8e4bf2a2c.png)

1.  Right click the project ‘get-started-rest-app’ \> Debug As \> Debug on
    Server and wait for Tomcat to start and load your project.

2.  Change the opened browser URL =
    <http://localhost:8080/get-started-rest-app/api/banner/default> so that it
    will call to BannerRestController.findById method.

3.  Eclipse IDE will prompt for perspective switch confirmation

4.  Tick remember my decision then click Switch

5.  Eclipse IDE will show the Debug perspective

![](get-started-1/2a5a71527b90fd55e6a4d98627213d66.png)

1.  Press F5 to Step In, F6 to Step Over, F7 to Return and F8 to Resume

### Running TFNG Get Started Banner and Greeting Library with Embedded Tomcat

1.  Expand project get-started-rest-banner \> src/main/java \>
    tfng.getstarted.banner

2.  Right click BannerRunner.java \> Run As \> Java Application and wait for
    embedded Tomcat to start.

![](get-started-1/605f59ecfb5f36fa5b9bd33cb1cde5d8.png)

1.  Open Firefox and change the URL = <http://localhost:8080/api/banner/default>
    to test the Banner REST GET.

2.  Click Stop to shut down the embedded Tomcat.

3.  Expand project get-started-rest-greeting \> src/main/java \>
    tfng.getstarted.greeting

4.  Right click GreetingRunner.java \> Run As \> Java Application and wait for
    embedded Tomcat to start.

5.  Open Firefox and change the URL = <http://localhost:8080/api/greetings> to
    test the Greeting REST GET.

### REST API Documentation

1.  Right click the project ‘get-started-rest-app’ \> Run As \> Run on Server

2.  Change the URL =
    <http://localhost:8080/get-started-rest-app/swagger-ui.html> to get a
    complete REST API documentation for the project.

![](get-started-1/0715e824b7b2f434627e2e85a5493571.png)

Deploy and Run TFNG Get Started REST Application in Tomcat
----------------------------------------------------------

### Deploy TFNG Get Started REST Application

1.  Copy previously build WAR file
    C:\\Projects\\TFNG\\get-started\\get-started-rest\\get-started-rest-projects\\get-started-rest-app\	arget\\get-started-rest-app-1.0.war
    to C:\\Programs\\Tomcat-9.0\\webapps

2.  Extract the WAR file get-started-rest-app-1.0.war into
    C:\\Programs\\Tomcat-9.0\\webapps\\get-started-rest-app

![](get-started-1/86762fb2d7e51dc7a916c7c147959c40.png)

### Configure Tomcat

1.  Open file tomcat-users.xml in C:\\Programs\\Tomcat-9.0\\conf with preferable
    Notepad++ or Notepad

2.  Modify as below then save

![](get-started-1/1f0f43c071e9d16d6fca4fbe0d61e026.png)

### Run Tomcat

1.  Open a Command Prompt

2.  Enter ‘CD /d C:\\Programs\\Tomcat-9.0’

3.  Enter ‘set JRE_HOME=C:\\Program Files\\Java\\jdk1.8’

4.  Enter ‘bin\\startup.bat’, then Windows will prompt Firewall permission for
    Java, click OK

![](get-started-1/570b8a49120b37c0e7b26d17f41e91d4.png)

1.  Open Firefox and change the opened browser URL =
    <http://localhost:8080/get-started-rest-app/api/banner/default>.

2.  Press CTRL+C to shut down Tomcat

Deploy and Run TFNG Get Started REST Application to Heroku Cloud
----------------------------------------------------------------

1.  Register an account with <https://signup.heroku.com/?c=70130000001x9jFAAQ>

2.  Download and Install [Heroku
    CLI](https://cli-assets.heroku.com/branches/stable/heroku-windows-amd64.exe)

3.  Open Command Prompt then enter ‘heroku plugins:install heroku-cli-deploy’ to
    install Heroku CLI deployment plugin

### Create a Heroku Cloud Application

1.  Enter ‘heroku login’, then enter email and password

2.  Enter ‘heroku create –no-remote tfng-get-started-rest’ to create a Heroku
    application without Git repository and wait for the application to be
    created

![](get-started-1/2d2a1139b014c302933e66010259cead.png)

### Deploy TFNG Get Started REST Application to Heroku Cloud

1.  Enter ‘heroku war:deploy
    C:\\Projects\\TFNG\\get-started\\get-started-rest\\get-started-rest-projects\\get-started-rest-app\	arget\\get-started-rest-app-1.0.war
    --app tfng-get-started-rest’

![](get-started-1/61e39bdfa457b7d495b67347639b36d1.png)

1.  Open Firefox and change URL =
    <https://tfng-get-started-rest.herokuapp.com/api/greetings> to test your
    cloud application

References
----------

<https://www.baeldung.com/rest-with-spring-series/>

<https://www.baeldung.com/exception-handling-for-rest-with-spring>

<https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api>

<https://dzone.com/articles/spring-boot-heroku-and-cicd>
