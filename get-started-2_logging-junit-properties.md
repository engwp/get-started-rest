TFNG Get Started 2
==================

Get Started with Logging, JUnit and Properties

Logging
-------

### What is SLF4J and Logback

<https://www.baeldung.com/slf4j-with-log4j2-logback>

<https://www.baeldung.com/logback>

### Logging Level

1.  FATAL should be reserved for errors that cause the application to crash or
    fail to start (ex: JVM out of memory)

2.  ERROR should contain technical issues that need to be resolved for proper
    functioning of the system (ex: couldn’t connect to database)

3.  WARN is best used for temporary problems or unexpected behavior that does
    not significantly hamper the functioning of the application (ex: failed user
    login)

4.  INFO should contain messages that describe what is happening in the
    application (ex: user registered, order placed)

5.  DEBUG is intended for messages that could be useful in debugging an issue
    (ex: method execution started)

6.  TRACE is like DEBUG but contains more detailed events (ex: data model
    updated). Trace-level logging should be used only for tracing the flow of
    execution through a program and for flagging positions in a program (i.e. to
    ensure they have been reached)

### Logging Appender

<https://logback.qos.ch/manual/appenders.html>

1.  ConsoleAppender – append logging events into the console

2.  RollingFileAppender – append logging events into a file with the capability
    to rollover to another file once a certain condition like maximum file size
    or time limit is met.

3.  DBAppender – appends logging events into 3 database tables

    1.  The logging_event table:

![Logging Event table](get-started-2/d19f63fba538f93192e868e972311b41.gif)

1.  The logging_event_exception table:

![Logging Event Exception table](get-started-2/85169f09f7427087f9e37062786909ac.gif)

1.  The logging_event_property table:

![Logging Event Property table](get-started-2/8aff80d0f4a540e361f9bbb0e9a9d37a.gif)

### Logging in Spring Boot

<https://www.baeldung.com/spring-boot-logging>

<https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-logging.html>

<https://springframework.guru/using-yaml-in-spring-boot-to-configure-logback/>

### Logger Instance

1.  Each class containing calls to logging system must have its own logger
    instance and is not sharing with other classes

2.  Each logger instance should be static and final

3.  Each logger instance should be named the same throughout the project

![](get-started-2/1a865d1fdc3590cf9f492cf5b3883473.png)

### Logging best Practices

<https://wiki.opendaylight.org/view/BestPractices/Logging_Best_Practices>

<https://stackify.com/9-logging-sins-java/amp/>

Junit
-----

<https://www.baeldung.com/junit>

<https://www.baeldung.com/java-junit-hamcrest-guide>

<https://www.baeldung.com/bdd-mockito>

<https://www.baeldung.com/spring-boot-testing>

<https://www.baeldung.com/integration-testing-a-rest-api>

<https://www.blazemeter.com/blog/spring-boot-rest-api-unit-testing-with-junit>

<https://dzone.com/articles/unit-testing-best-practices>

### Test Class and Method

1.  Name your test class = Target class + ‘Test’

    1.  Test class for ‘WebApplication.java’ should be named
        ‘WebApplicationTest.java’

2.  Name your test method = ‘when’ + Do something + ‘_given’ + Some conditions +
    ‘_will’ + Return something

    1.  Positive test method for GreetingService.findById(id) should be named
        ‘whenFindById_givenGreeting_willReturnGreeting’

    2.  Negative test method for GreetingService.findById(id) should be named
        ‘whenFindById_givenNoGreeting_willReturnNotFound’

![](get-started-2/c74088e4dadf7a85c9354ab168870f8a.png)

### Run JUnit Test

1.  Right click get-started-rest-app project \> Run As \> JUnit Test and wait
    for JUnit to complete all tests

![](get-started-2/c274d5fdd137ad27fd402259d507c6f9.png)

1.  Click JUnit tab in bottom dock

![](get-started-2/9af835e741158953ebd82330db9c92f4.png)

### Configure Static Members Import in Eclipse IDE

In Eclipse IDE menu, select Windows \> Preferences

Expand Java \> Editor \> Content Assist \> Favorites

Click New Type and add the below list:

1.  org.hamcrest.Matchers

2.  org.mockito.BDDMockito

3.  org.springframework.test.web.servlet.request.MockMvcRequestBuilders

4.  org.springframework.test.web.servlet.result.MockMvcResultMatchers

5.  com.jayway.jsonassert.JsonAssert

### Maven Surefire Plugin

<https://www.baeldung.com/maven-surefire-plugin>

1.  Surefire will include all test classes with the following patterns:

    1.  Test\*.java

    2.  \*Test.java

    3.  \*TestCase.java

Properties
----------

<https://www.baeldung.com/properties-with-spring>

<https://docs.spring.io/spring-boot/docs/current/reference/html/howto-properties-and-configuration.html>

### Properties in Spring Boot

1.  Spring Boot loads either application.properties or application.yml (not
    both) from src/main/resources directory

2.  Properties configured in our application.yml file will override the default
    properties in Spring Boot

### Configure H2 Database Properties in application.yml

<http://www.h2database.com/html/main.html>

1.  Modify the application.yml file in get-started-rest-app project under
    src/main/resources as below to change the database name =
    get-started-rest-app and enable H2 database console

![](get-started-2/b8691367f889c03fd1f26ea24c6a1c61.png)

1.  Run get-started-rest-app project as Server

2.  Change the browser URL = <http://localhost:8080/get-started-rest-app/h2>,
    then Enter as below:

    1.  JDBC URL = jdbc:h2:mem:get-started-rest-app

    2.  User Name = admin

    3.  Password = admin

![](get-started-2/d237e5cc8a123c59630526ce5e74c5b8.png)

1.  Click on Connect

2.  Run SQL statement ‘select \* from “schema_version”’ to view executed Flyway
    Database Migration version

![](get-started-2/b10e7a95fdb889b7e49798886b1bca71.png)

1.  Run SQL statement ‘select \* from greeting’ to view inserted greeting
    records

![](get-started-2/9d169fc5307c7034ec2b628bdd4f6516.png)

### Configure Logging Level Properties in application.yml

1.  Modify the application.yml file in get-started-rest-app project under
    src/main/resources as below to change the logging level for package
    org.hibernate and tfng.getstarted.rest to DEBUG

![](get-started-2/914805bc5b41fc712be680053856c7fa.png)

1.  Run get-started-rest-app project as Server

2.  Change the browser URL =
    <http://localhost:8080/get-started-rest-app/api/greetings> , then press
    Enter

3.  You will see DEBUG level logging for package org.hibernate and
    tfng.getstarted.rest

![](get-started-2/4eab6c304a0bb61f3edbd99174b7f3d8.png)
