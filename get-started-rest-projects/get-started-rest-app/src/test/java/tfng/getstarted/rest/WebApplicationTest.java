package tfng.getstarted.rest;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class WebApplicationTest {
  
  @Autowired
  private MockMvc mvc;

  @Test
  public void whenGetHealth_willReturnStatusUp() throws Exception {
    //when
    final ResultActions result = mvc.perform(
        get("/actuator/health").contentType(MediaType.APPLICATION_JSON_UTF8));

    //will
    result.andExpect(status().isOk());
    result.andExpect(jsonPath("$.status", equalTo("UP")));
  }
    
}
