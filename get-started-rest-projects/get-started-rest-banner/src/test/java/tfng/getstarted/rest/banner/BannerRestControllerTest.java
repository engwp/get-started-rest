package tfng.getstarted.rest.banner;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.willReturn;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureRestDocs
public class BannerRestControllerTest {
  
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private BannerService bannerService;
    
  @Test
  public void whenFindById_givenBanner_willReturnBanner() throws Exception {
    //given
    willReturn(Optional.of(new Banner("test-banner-message")))
        .given(bannerService)
        .findById("default");
    
    //when
    final ResultActions result = mockMvc.perform(
        get("/api/banners/default").contentType(MediaType.APPLICATION_JSON_UTF8));

    //will
    result.andExpect(status().isOk());
    result.andExpect(jsonPath("$.message", equalTo("test-banner-message")));
    
    //generate
    result.andDo(document("banner/findById"));
  }

}
