package tfng.getstarted.rest.banner;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

@Service
public class BannerService {

  private static final Logger LOGGER = LoggerFactory.getLogger(BannerService.class);
  
  private MessageSourceAccessor bannerMessages;
  
  @Autowired
  private BuildProperties buildProperties;
  
  /**
   * <p>Find a {@link Banner} by id.</p>
   * @param id the id to find
   * @return
   */
  public Optional<Banner> findById(final String id) {
    
    LOGGER.debug("Getting banner message with id={}, providing args = {}, {}", 
        id, buildProperties.getName(), buildProperties.getVersion());
    final Banner banner = new Banner();
    banner.setMessage(bannerMessages.getMessage(id, 
        new Object[] { buildProperties.getName(), buildProperties.getVersion() }));
    LOGGER.debug("Returning banner with message = {}", banner.getMessage());
    
    return Optional.of(banner);
  }
  
  @Autowired
  public void setBannerMessageSource(final BannerMessageSource bannerMessageSource) {
    bannerMessages = new MessageSourceAccessor(bannerMessageSource);
  }

}
