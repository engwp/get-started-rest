package tfng.getstarted.rest.banner;

import org.apache.commons.lang3.ClassUtils;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class BannerMessageSource
    extends ResourceBundleMessageSource {

  public BannerMessageSource() { 
    super.setBasename(ClassUtils.getPackageName(BannerMessageSource.class) 
        + "." + ClassUtils.getSimpleName(Banner.class));
  }
    
}
