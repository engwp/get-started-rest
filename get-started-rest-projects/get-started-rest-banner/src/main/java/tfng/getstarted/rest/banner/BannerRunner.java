package tfng.getstarted.rest.banner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BannerRunner {

  public static void main(final String[] args) {
    SpringApplication.run(BannerRunner.class, args);
  }

}
