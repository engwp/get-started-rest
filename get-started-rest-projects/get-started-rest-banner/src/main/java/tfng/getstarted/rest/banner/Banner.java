package tfng.getstarted.rest.banner;

import java.io.Serializable;

public class Banner 
    implements Serializable {

  private static final long serialVersionUID = 1L;

  private String message;
  
  public Banner() {
  }
  
  public Banner(final String message) {
    this.message = message;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }

}
