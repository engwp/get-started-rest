package tfng.getstarted.rest.greeting;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class GreetingService {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(GreetingService.class);

  @Autowired
  private GreetingGateway greetingGateway;
  
  /**
   * Return all found {@link Greeting}.
   * @param pageable pagination information
   * @return
   */
  public Slice<Greeting> findAll(final Pageable pageable) {
    LOGGER.debug("Finding all greetings with page = {}, size = {}.", 
        pageable.getPageNumber(), pageable.getPageSize());
    return greetingGateway.findAll(pageable);
  }
  
  /**
   * Return all found {@link Greeting} containing message, ignoring case.
   * @param pageable pagination information
   * @param message message to search
   * @return
   */
  public Slice<Greeting> findAllByMessageContainingIgnoreCase(final Pageable pageable, 
      final String message) {
    LOGGER.debug("Finding all greetings by containing message" 
        + ", ignoring case, with page = {}, size = {}, message = {}.", 
        pageable.getPageNumber(), pageable.getPageSize(), message);
    return greetingGateway.findAllByMessageContainingIgnoreCase(pageable, message);
  }
  
  public Optional<Greeting> findById(final Integer id) {
    return greetingGateway.findById(id);
  }

}
