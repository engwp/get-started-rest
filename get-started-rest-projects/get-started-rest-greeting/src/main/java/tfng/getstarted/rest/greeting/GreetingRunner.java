package tfng.getstarted.rest.greeting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreetingRunner {

  public static void main(final String[] args) {
    SpringApplication.run(GreetingRunner.class, args);
  }

}
