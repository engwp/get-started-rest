package tfng.getstarted.rest.greeting;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Greeting 
    implements Serializable {

  private static final long serialVersionUID = 1L;
      
  @Id
  @GeneratedValue
  private Integer id;
      
  private String message;

  public Greeting() {
  }

  public Greeting(final Integer id, final String message) {
    this.id = id;
    this.message = message;
  }
      
  public Integer getId() {
    return id;
  }
      
  public void setId(Integer id) {
    this.id = id;
  }
      
  public String getMessage() {
    return message;
  }
      
  public void setMessage(String message) {
    this.message = message;
  }

}
