package tfng.getstarted.rest.greeting;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/greetings")
public class GreetingRestController {

  @Autowired
  private GreetingService greetingService;
  
  @GetMapping(value = "")
  public Slice<Greeting> findAll(final Pageable pageable) {
    return greetingService.findAll(pageable);
  }
  
  @GetMapping(value = "", params = "message")
  public Slice<Greeting> findAll(final Pageable pageable, @RequestParam String message) {
    return greetingService.findAllByMessageContainingIgnoreCase(pageable, message);
  }
  
  @GetMapping(value = "/{id}")
  public Optional<Greeting> findById(@PathVariable final Integer id) {
    return greetingService.findById(id);
  }

}
