package tfng.getstarted.rest.greeting;

import org.springframework.data.repository.Repository;

public interface GreetingJpaGateway 
    extends GreetingGateway, Repository<Greeting, Integer> {

}
