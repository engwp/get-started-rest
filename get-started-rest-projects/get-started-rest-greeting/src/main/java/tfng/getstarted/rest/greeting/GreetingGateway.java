package tfng.getstarted.rest.greeting;

import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

public interface GreetingGateway {

  Slice<Greeting> findAll(Pageable pageable);
  
  Slice<Greeting> findAllByMessageContainingIgnoreCase(Pageable pageable, String message);
  
  Optional<Greeting> findById(Integer id);

}
