package tfng.getstarted.rest.greeting;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.willReturn;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.SliceImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureRestDocs
public class GreetingRestControllerTest {
  
  @Autowired
  private MockMvc mvc;
  
  @MockBean
  private GreetingService greetingService;

  @Test
  public void whenFindAll_givenGreeting_willReturnAll() throws Exception {
    //given
    willReturn(new SliceImpl<>(Arrays.asList(
            new Greeting(1, "test-message-1"), 
            new Greeting(2, "test-message-2"))))
        .given(greetingService)
        .findAll(PageRequest.of(0, 10));

    //when
    final ResultActions result = mvc.perform(
        get("/api/greetings")
        .param("page", "0")
        .param("size", "10")
        .contentType(MediaType.APPLICATION_JSON_UTF8));
    
    //will
    result.andExpect(status().isOk());
    result.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    result.andExpect(jsonPath("$.numberOfElements", equalTo(2)));
    result.andExpect(jsonPath("$.last", equalTo(true)));
    result.andExpect(jsonPath("$.content[0].id", equalTo(1)));
    result.andExpect(jsonPath("$.content[0].message", equalTo("test-message-1")));
    result.andExpect(jsonPath("$.content[1].id", equalTo(2)));
    result.andExpect(jsonPath("$.content[1].message", equalTo("test-message-2")));
    
    //generate
    result.andDo(document("greeting/findAll"));
  }

  @Test
  public void whenFindAllByMessageContainingIgnoreCase_givenGreeting_willReturnMatched() 
      throws Exception {
    //given
    willReturn(new SliceImpl<>(Arrays.asList(
            new Greeting(1, "test-message-foo-1"))))
        .given(greetingService)
        .findAllByMessageContainingIgnoreCase(PageRequest.of(0, 10), "foo");

    //when
    final ResultActions result = mvc.perform(
        get("/api/greetings")
        .param("page", "0")
        .param("size", "10")
        .param("message", "foo")
        .contentType(MediaType.APPLICATION_JSON_UTF8));
    
    //will
    result.andExpect(status().isOk());
    result.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    result.andExpect(jsonPath("$.numberOfElements", equalTo(1)));
    result.andExpect(jsonPath("$.last", equalTo(true)));
    result.andExpect(jsonPath("$.content[0].id", equalTo(1)));
    result.andExpect(jsonPath("$.content[0].message", equalTo("test-message-foo-1")));
    
    //generate
    result.andDo(document("greeting/findAllByMessageContainingIgnoreCase"));
  }
  
  @Test
  public void whenFindById_givenGreeting_willReturnOne() throws Exception {
    //given
    willReturn(Optional.of(new Greeting(1, "test-message")))
        .given(greetingService)
        .findById(1);
    
    //when
    final ResultActions result = mvc.perform(get("/api/greetings/1")
        .contentType(MediaType.APPLICATION_JSON_UTF8));
    
    //will
    result.andExpect(status().isOk());
    result.andExpect(jsonPath("$.id", equalTo(1)));
    result.andExpect(jsonPath("$.message", equalTo("test-message")));
    
    //generate
    result.andDo(document("greeting/findById"));
  }

}
